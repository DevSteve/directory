var myControllers = angular.module('myControllers', []);

myControllers.controller('SearchController', function MyController($scope, $http) {
	$http.get('js/data.json').then(function(response) {
		$scope.reviews = response.data;
		$scope.reviewOrder = 'game';

		$scope.getStars = function(reviews) {
			// Get the value
			var val = parseFloat(reviews);
			// Turn value into number/100
			var size = val / 5 * 100;
			return size + '%';
		}
	});
});

myControllers.controller('DetailsController', function MyController($scope, $http, $routeParams) {
	$http.get('js/data.json').then(function(response) {
		$scope.reviews = response.data;
		$scope.whichItem = $routeParams.itemId;

		$scope.getStars = function(reviews) {
			// Get the value
			var val = parseFloat(reviews);
			// Turn value into number/100
			var size = val / 5 * 100;
			return size + '%';
		}
		
		if ($routeParams.itemId > 0) {
			$scope.prevItem = Number($routeParams.itemId) - 1;
		} else {
			$scope.prevItem = $scope.reviews.length - 1;
		}

		if ($routeParams.itemId < $scope.reviews.length - 1) {
			$scope.nextItem = Number($routeParams.itemId) + 1;
		} else {
			$scope.nextItem = 0;
		}
	});
});